# hptts - Haskell Project TimeTracking System

This is HPTTS, a web-based time tracking system, written in Haskell using Servant and Persistent.
The project structure is based upon Matt Parsons' [servant-persistent] example.

This project was done as a project for AFP at UU, by:
- Lucas Dresscher (5750865)
- Rick van Hoef (5665299)
- Maarten van den Berg (5636450)

## Getting started
HPTTS needs to have access to a running instance of Postgres, where it can create its tables.
The easiest way to do this is by using `docker-compose`.

To start, run `docker-compose up -d`. This launches a Postgres instance and a management interface.
To begin, either create an admin user manually, or import the example database using the command below.

The database can be managed by opening this link in a browser (default password: `insanelysecret`):
http://localhost:8088/?pgsql=database&username=hptts&db=hptts&ns=public

A example database is included in `example-database.sql`. It can be imported by running `docker-compose run database psql -h database -p 5432 -U hptts < example-database.sql`.

When the database is running, you can `stack run` to start the server and perform migrations.

## The API:
The API's endpoints are documented using [Postman].
The documentation is [available online][documenter], and included as a Postman collection in `postman.json`.

[Postman]: https://getpostman.com
[documenter]: https://documenter.getpostman.com/view/6289532/S1ETRGcg

[servant-persistent]: https://github.com/parsonsmatt/servant-persistent

[API]: 		 	./docs/API.md
[STRUCTURE]: 	./docs/STRUCTURE.md

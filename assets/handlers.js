/* Handlers for filling input fields */
var getEntityNames = function(res, element, entityName) {
    if (res == null) { return; }

    for (i = 0; i < res.length; i++) {
        var newEntityOption = document.createElement(entityName + i);
        var name = res[i].name;

        newEntityOption.appendChild(document.createTextNode(name));
        newEntityOption.value = name;

        element.appendChild(newEntityOption);
    }
}

var getEntryGroupsHandler = function(res) {
    var element = document.getElementById("inputEntryGroup");
    var entityName = "group";
    getEntityNames(res, element, entityName);
}

var getEntryCategoriesHandler = function(res) {
    var element = document.getElementById("inputEntryCategory");
    var entityName = "category";
    getEntityNames(res, element, entityName);
}

var getEntryLocationsHandler = function(res) {
    var element = document.getElementById("inputEntryLocation");
    var entityName = "location";
    getEntityNames(res, element, entityName);
}

/* Handlers for creating new entities */
var getEntityHandler = function(res) {
    return res;
}

var createUserJSBody = function(userName, userEmail, userPassword) {
    var userJSObject = { name: userName, email: userEmail, passwordHash: userPassword, admin: false };
    return userJSObject;
}

var createGroupJSBody = function(groupName) {
    var groupJSObject = { name: groupName };
    return groupJSObject;
}

var createCategoryJSBody = function(mainCategoryName, subCategoryName) {
    var categoryJSObject;
    if (subCategoryName == null | subCategoryName === '') {
        categoryJSObject = { name: mainCategoryName };
        return categoryJSObject;
    }
    var parentCategoryId = getCategoriesByName(mainCategoryName, getEntityHandler, alert).id;
    categoryJSObject = { name: subCategoryName, parentId: parentCategoryId }
    return categoryJSObject;
}

var createLocationJSBody = function(locationName) {
    var locationJSObject = { name: locationName };
    return locationJSObject;
}


var createLoginJSBody = function(username, password) {
    var loginJSObject = { username: username, password: password };
    return loginJSObject;
}

var createEntryJSBody = function(startTime, endTime, description, categoryId, locationId) {
    var locationJSObject = { startTime: startTime + 'Z', endTime: endTime + 'Z', description: description, categoryId: Number(categoryId), locationId: Number(locationId) };
    return locationJSObject;
}

/* Handlers for getting totals */
// Inspired by: https://www.geeksforgeeks.org/javascript-math-random-function/
// var getRandomEntityId = function(getEntityFunction) {
//     var entities = getEntityFunction();

//     var min = 0;
//     var max = entities.length - 1;
//     var randomInt = Math.floor(Math.random() * (+max - +min)) + +min;
    
//     return entities[randomInt].id;
// }

var fillOverviewUsers = function(users) {
    var usersTableBody = document.getElementById("overview_users_tbody");

    // Create new row
    for (var i in users) {
        var newUserRow = usersTableBody.insertRow(-1);

        // User name column
        var userNameCol = newUserRow.insertCell(0);
        var userName = document.createElement(users[i].name);
        userNameCol.appendChild(userName);
        // User email column
        var userEmailCol = newUserRow.insertCell(1);
        var userEmail = document.createElement(users[i].email);
        userEmailCol.appendChild(userEmail);
    }
}

// var fillOverviewCategories = function(categories) {
//     var categoriesTableBody = document.getElementById("overview_categories_body");

//     for (var i in categories) {
//         // Create new row
//         var newCategoryRow = categoriesTableBody.insertRow(-1);

//         // Category name column
//         var categoryNameCol = newCategoryRow.insertCell(0);
//         var categoryName = document.createElement(categories[i].name);
//         categoryNameCol.appendChild(categoryName);
//         // Category type (sub or main)
//         var categoryTypeCol = newCategoryRow.insertCell(1);
//         var type;
//         if (categories[i].parentId == null) {
//             type = "Main category"
//         }
//         else { type = "Sub category" }
//         var categoryType = document.createElement(type);
//         categoryTypeCol.appendChild(categoryType);
//     }
// }

// var fillOverviewLocations = function(locations) {
//     var locationsTableBody = document.getElementById("overview_locations_tbody");

//     // Create new row
//     for (var i in locations) {
//         var newLocationRow = locationsTableBody.insertRow(-1);

//         // Location name column
//         var locationNameCol = newLocationRow.insertCell(0);
//         var locationName = document.createElement(locations[i].name);
//         locationNameCol.appendChild(locationName);
//     }
// }

var getTotalsUserHandler = function(totals) {
    var usersTableBody = document.getElementById("overview_locations_tbody");

    // Create new row
    for (var i in totals) {
        var newTotalsRow = usersTableBody.insertRow(1);
        
        // Columns
        for (var j in totals[i]) {
            // TODO: how does this total element look like?
            // var locationNameCol = newLocationRow.insertCell(0);
            // var locationName = document.createElement(locations[i].name);
            // locationNameCol.appendChild(locationName);
        }
    }
}

// var getTotalsCategoryHandler = function(res) {
//     var element = document.getElementById("totalsRandomCategory");
//     element.value = res[0]; // TODO: Does this work?
// }

// var getTotalsLocationHandler = function(res) {
//     var element = document.getElementById("totalsRandomLocation");
//     element.value = res[0]; // TODO: Does this work?
// }

var setLoginCookie = function(JWT) {
    if (JWT) {
        document.cookie = "sessionId=" + JWT;
    }
}

// from https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript#24103596
function getLoginCookie() {
    var nameEQ = "sessionId=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

var fillDropdown = function(parent, objects) {
    var dropdown = document.getElementById(parent);

    for (var i in objects) {
        var node = document.createElement('option');
        node.value = objects[i].id;
        node.innerHTML = objects[i].name;
        dropdown.appendChild(node);
    }
}

var fillEntryGroups = function(groups) {
    fillDropdown('inputEntryGroupName', groups);
}

var fillEntryCategories = function(categories) {
    fillDropdown('inputEntryCategory', categories)
}

var fillEntryLocations = function(locs) {
    fillDropdown('inputEntryLocation', locs);
}

var fillEntryUsers = function(users) {
    fillDropdown('inputEntryUsers', users);
}

var fillOverviewUsers = function(users) {
    var table = document.getElementById('overview_users_tbody');

    users.forEach(function (user) {
        getTotals(Number(user.id), null, null, null, null, function (x) {
            createUserRow(table, user, x / 60);
        }
        , alert);
    })
}

var createUserRow = function(table, obj, total) {
    var row = document.createElement('tr');
        
    row.appendChild(createCell(obj.name));
    row.appendChild(createCell(obj.email));

    row.appendChild(createCell(total));

    table.appendChild(row);
}

var createCell = function(x) {
    cell = document.createElement("td");
    text = document.createTextNode(x);
    cell.appendChild(text);
    return cell;
}

var fillOverviewCategories = function(cats) {
    var table = document.getElementById('overview_categories_tbody');

    cats.forEach(function (cat) {
        getTotals(null, Number(cat.id), null, null, null, function (x) {
            createCatRow(table, cat, x / 60);
        }
        , alert);
    })
}


var createCatRow = function(table, cat, total) {
    var row = document.createElement('tr');
          
    row.appendChild(createCell(cat.name));
    row.appendChild(createCell(cat.parentId == null? "Main category" : "Subcategory"));

    row.appendChild(createCell(total));

    table.appendChild(row);
}

var fillOverviewLocations = function(locs) {
    var table = document.getElementById('overview_locations_tbody');

    locs.forEach(function (loc) {
        getTotals(null, null, Number(loc.id), null, null, function (x) {
            createLocRow(table, loc, x / 60);
        }
        , alert);
    })
}

var createLocRow = function(table, loc, total) {
    var row = document.createElement('tr');
        
    row.appendChild(createCell(loc.name));

    row.appendChild(createCell(total));

    table.appendChild(row);
}


var getGroups = function(onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/groups', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

var getGroupsById = function(id, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/groups/' + encodeURIComponent(id) + '', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

var getGroupsByName = function(name, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/groups/' + encodeURIComponent(name) + '', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

var postGroups = function(body, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/groups', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(JSON.stringify(body));
};

var deleteGroupsById = function(id, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('DELETE', '/groups/' + encodeURIComponent(id) + '', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

var getGroupsByIdMembers = function(id, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/groups/' + encodeURIComponent(id) + '/members', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

var postGroupsByIdMembersById = function(id, id, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/groups/' + encodeURIComponent(id) + '/members/' + encodeURIComponent(id) + '', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

var deleteGroupsByIdMembersById = function(id, id, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('DELETE', '/groups/' + encodeURIComponent(id) + '/members/' + encodeURIComponent(id) + '', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};


var getEntries = function(onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/entries', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

var getEntriesById = function(id, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/entries/' + encodeURIComponent(id) + '', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

var postEntries = function(users, body, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/entries', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) {
          for (var i in users) {
            postEntriesByIdParticipantsById(res.id, Number(users[i].value), alert, alert);
          }
          onSuccess(res);
        }
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(JSON.stringify(body));
};

var deleteEntriesById = function(id, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('DELETE', '/entries/' + encodeURIComponent(id) + '', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

var getEntriesByIdParticipants = function(id, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/entries/' + encodeURIComponent(id) + '/participants', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

var postEntriesByIdParticipantsById = function(id, id, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/entries/' + encodeURIComponent(id) + '/participants/' + encodeURIComponent(id) + '', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

var deleteEntriesByIdParticipantsById = function(id, id, onSuccess, onError) {
  var xhr = new XMLHttpRequest();
  xhr.open('DELETE', '/entries/' + encodeURIComponent(id) + '/participants/' + encodeURIComponent(id) + '', true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        if (res) onError(res);
      }
    }
  };
  xhr.send(null);
};

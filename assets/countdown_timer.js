// Set the date we're counting down to
var countDownDate = new Date("Jun, 2019 23:59:59").getTime();

window.onload = function showCountdown(){
    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="countdownClock"
    document.getElementById("countdown_clock").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";
    setTimeout(showCountdown, 1000);
}

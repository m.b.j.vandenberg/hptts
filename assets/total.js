
var getTotals = function(user, category, location, from, to, onSuccess, onError) {
  var xhr = new XMLHttpRequest();

  var users = user === null? "" : 'user=' + Number(user) + '&';
  var cats = category === null? "" : 'category=' + Number(category) + '&';
  var locs = location === null? "" : 'location=' + Number(location) + '&';
  var froms = from === null? "" : 'from=' + encodeURIComponent(from) + '&';
  var tos = to === null? "" : 'to=' + encodeURIComponent(to) + '&';

  var querystring = ('/totals?' + users + cats + locs + froms + tos)
  querystring = querystring.substring(0, querystring.length - 1);

  xhr.open('GET', querystring, true);
  xhr.setRequestHeader('Authorization', "Bearer " + getLoginCookie())
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.onreadystatechange = function () {
    var res = null;
    if (xhr.readyState === 4) {
      if (xhr.status === 204 || xhr.status === 205) {
        onSuccess();
      } else if (xhr.status >= 200 && xhr.status < 300) {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        onSuccess(res);
      } else {
        try { res = JSON.parse(xhr.responseText); } catch (e) { onError(e); }
        onError(res);
      }
    }
  };
  xhr.send(null);
};

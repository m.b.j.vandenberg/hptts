# The API
- GET `/users` returns a list of all users in the database
- GET `/users/:name` returns the first user whose name is `:name`, and returns 404 if the user doesn't show up.
- POST `/users` with JSON like `{ "name": "String", "email": "String" }` to create a User.

## Playing with the API from the command line

Once the compiled `perservant` binary is running, you can use `curl` like below to play with the API from the command line.

```
# create a new user
$ curl --verbose --request POST --header "Content-Type: application/json" \
    --data '{"name": "foo", "email": "foo@foo.com"}' \
	http://localhost:8081/users

# get all users in database
$ curl --verbose --request GET --header "Content-Type: application/json" \
	http://localhost:8081/users

# get certain user in database
$ curl --verbose --request GET --header "Content-Type: application/json" \
	http://localhost:8081/users/foo
```

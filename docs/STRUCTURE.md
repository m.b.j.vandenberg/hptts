# Project structure

## src/Main.hs

`main` starts off by pulling some settings from the environment, creating a connection pool, running the migrations, and finally running the app.

## src/Api.hs

This source contains the actual API definition.

## src/Api/*.hs

Contain the implementations of the APIs for each model.

## src/Config.hs

Contains the `runDb`, `makePool`, and `Config` definitions.

## src/Models.hs

Contains fairly typical Persistent schema definitions.

{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Models where

import           Control.Monad.Reader (MonadIO, MonadReader, asks, liftIO)
import           Database.Persist.Sql
import           GHC.Generics

import           Database.Persist.TH  (mkDeleteCascade, mkMigrate, mkPersist,
                                       persistLowerCase, share, sqlSettings)

import           Data.Aeson.Types     (FromJSON, ToJSON)

import           Config               (Config, configPool)
import           Data.Text            (Text)
import           Data.Time.Clock      (UTCTime)

import           Servant.Auth.Server  (FromJWT, ToJWT)

share [mkPersist sqlSettings, mkDeleteCascade sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|

-- | Represents a User, which may be a `Member` of `Group`s, and be a
-- `Participant` in an `Activity`.
User json
    name               Text   -- ^ The User's full name.
    email              Text   -- ^ The User's email address.
    admin              Bool   -- ^ Whether or not the User has admin rights.
    passwordHash       Text   -- ^ A BCrypted hash of the User's password.
    UniqueUserName     name   -- ^ Database constraint restricting the database to at most one User with a given name.
    UniqueEmail        email  -- ^ Database constraint restricting the database to at most one User with a given email address.

-- | Represents a group of multiple `User`s, which can be selected easily to
-- assign them to an `Entry`.
Group json
    name               Text -- ^ The Group's name.
    UniqueGroupName    name -- ^ Database constraint restricting the database to at most one Group with the given name.

-- | Represents the relation between `User`s and `Group`s.
Member json
    userId             UserId  -- ^ Foreign key to the contained User.
    groupId            GroupId -- ^ Foreign key to the containing Group.
    UniqueMember       userId groupId  -- ^ Database constraint restricting the database to at most one link between a Group and a User.

-- | Represents a category, in which Entries can be grouped. Categories can contain other categories.
Category json
    name               Text             -- ^ The Category's name
    parentId           CategoryId Maybe -- ^ Nullable reference to a parent Category.
    UniqueCategoryName name             -- ^ Constrains Categories to contain at most one category with a given name.

-- | Represents a location where Entries can be connected to.
Location json
    name               Text -- ^ The name of the Location
    UniqueLocationName name -- ^ Constrains Locations to contain at most one location with a given name.

-- | Represents the relation between Entries and Users.
Participant json
    userId             UserId         -- ^ Reference to the user.
    entryId            EntryId        -- ^ Reference to an Entry.
    UniqueParticipant  userId entryId -- ^ Constrains Participants to at most one Participant per User-Entry pair.

-- | Represents an entry in the work log, showing how many hours were spent.
-- References a category and location for grouping purposes.
Entry json
    startTime          UTCTime    -- ^ When the work started.
    endTime            UTCTime    -- ^ When the work ended.
    description        Text       -- ^ Description of the work that was done.
    categoryId         CategoryId -- ^ Reference to a Category.
    locationId         LocationId -- ^ Reference to a Location.
|]

-- JSON object embedded in the token issued to users.
data JWTData = JWTData { userid :: UserId, username :: Text, useremail :: Text, isadmin :: Bool }
  deriving (Eq, Show, Read, Generic)

instance ToJSON JWTData
instance ToJWT JWTData
instance FromJSON JWTData
instance FromJWT JWTData

-- Converts the data in a User to data suitable for embedding in a JWT token.
user2JWT :: Entity User -> JWTData
user2JWT u = JWTData {
    userid = uid,
    username = userName uv,
    useremail = userEmail uv,
    isadmin = userAdmin uv
  }
  where uv  = entityVal u
        uid = entityKey u

-- | BCrypt hash of "extrasecret".
defaultPassword :: Text
defaultPassword = "$2b$04$TDGZfeTwTP8B.xs/mfEh6.cMfQ2JWTN/YpghKngACZn4fF2LMKttW"

doMigrations :: SqlPersistT IO ()
doMigrations = runMigration migrateAll

runDb :: (MonadReader Config m, MonadIO m) => SqlPersistT IO b -> m b
runDb query = do
    pool <- asks configPool
    liftIO $ runSqlPool query pool

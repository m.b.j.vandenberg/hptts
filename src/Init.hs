{-# LANGUAGE OverloadedStrings #-}

module Init where

import           Control.Concurrent          (killThread)
import           Database.Persist.Postgresql (runSqlPool)
import           Network.Wai                 (Application)
import           System.Environment          (lookupEnv)
import           System.Remote.Monitoring    (forkServer, serverThreadId)

import           Api                         (app)
import           Api.Auth                    (generateJavaScript)
import           Config                      (Config (..), Environment (..),
                                              makePool, setLogger)
import           Control.Exception           (bracket)
import           Crypto.JOSE.JWK             (JWK)
import           Data.Aeson                  (decodeFileStrict, encodeFile)
import           Data.Maybe                  (fromJust)
import qualified Data.Pool                   as Pool
import qualified Katip
import           Logger                      (defaultLogEnv)
import           Models                      (doMigrations)
import           Network.Wai.Handler.Warp    (run)
import           Safe                        (readMay)
import           Servant.Auth.Server         (defaultJWTSettings, generateKey)
import           System.Directory            (doesFileExist)

-- | An action that creates a WAI 'Application' together with its resources,
--   runs it, and tears it down on exit
runApp :: IO ()
runApp = bracket acquireConfig shutdownApp runApp'
  where
    runApp' config = run (configPort config) =<< initialize config

-- | The 'initialize' function accepts the required environment information,
-- initializes the WAI 'Application' and returns it
initialize :: Config -> IO Application
initialize cfg = do
    let logger = setLogger (configEnv cfg)
    runSqlPool doMigrations (configPool cfg)
    generateJavaScript
    pure . logger . app $ cfg

-- | Allocates resources for 'Config'
acquireConfig :: IO Config
acquireConfig = do
    port <- lookupSetting "PORT" 8081
    env  <- lookupSetting "ENV" Development
    logEnv <- defaultLogEnv
    pool <- makePool env logEnv
    ekgServer <- forkServer "localhost" 8000

    -- JWT Key loading
    jwtKey <- loadJWK
    pure Config
        { configPool = pool
        , configEnv = env
        , configLogEnv = logEnv
        , configPort = port
        , configEkgServer = serverThreadId ekgServer
        , configJWTKey = jwtKey
        , configJWTSettings = defaultJWTSettings jwtKey
        }

jwtKeyFilePath :: FilePath
jwtKeyFilePath = "./.jwt-key"

-- | Load the JWT key if it exists, else generate one and save it.
loadJWK :: IO JWK
loadJWK = do
  haveFile <- doesFileExist jwtKeyFilePath
  case haveFile of
    True -> do
      key <- decodeFileStrict jwtKeyFilePath
      return (fromJust key)
    False -> do
      key <- generateKey
      _ <- encodeFile jwtKeyFilePath key
      return key

-- | Takes care of cleaning up 'Config' resources
shutdownApp :: Config -> IO ()
shutdownApp cfg = do
    _ <- Katip.closeScribes (configLogEnv cfg)
    Pool.destroyAllResources (configPool cfg)
    killThread (configEkgServer cfg)
    pure ()

-- | Looks up a setting in the environment, with a provided default, and
-- 'read's that information into the inferred type.
lookupSetting :: Read a => String -> a -> IO a
lookupSetting env def = do
    maybeValue <- lookupEnv env
    case maybeValue of
        Nothing ->
            return def
        Just str ->
            maybe (handleFailedRead str) return (readMay str)
  where
    handleFailedRead str =
        error $ mconcat
            [ "Failed to read [["
            , str
            , "]] for environment variable "
            , env
            ]

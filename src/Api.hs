{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Api (app) where

import           Control.Monad.Reader (runReaderT)
import           Servant              ((:<|>) (..), (:>), Proxy (Proxy), Raw,
                                       Server, serveDirectoryFileServer)
import           Servant.Auth.Server
import           Servant.Server

import           Api.Auth             (AuthAPI, authApi, authServer)
import           Api.Category         (CategoryAPI, categoryApi, categoryServer)
import           Api.Entry            (EntryAPI, entryApi, entryServer)
import           Api.Group            (GroupAPI, groupApi, groupServer)
import           Api.Location         (LocationAPI, locationApi, locationServer)
import           Api.Totals           (TotalsAPI, totalsApi, totalsServer)
import           Api.User             (UserAPI, userApi, userServer)
import           Models               (JWTData)

import           Config               (AppT (..), Config (..),
                                       configJWTSettings)

-- | This functions tells Servant how to run the 'App' monad with our
-- 'server' function for the user API.
appToServerUser :: Config -> AuthResult JWTData -> Server UserAPI
appToServerUser cfg auths = hoistServerWithContext userApi (Proxy :: Proxy '[JWT]) (convertApp cfg) (userServer auths)

appToServerGroup :: Config -> AuthResult JWTData -> Server GroupAPI
appToServerGroup cfg auths = hoistServerWithContext groupApi (Proxy :: Proxy '[JWT]) (convertApp cfg) (groupServer auths)

appToServerLocation :: Config -> AuthResult JWTData -> Server LocationAPI
appToServerLocation cfg auths = hoistServerWithContext locationApi (Proxy :: Proxy '[JWT]) (convertApp cfg) (locationServer auths)

appToServerCategory :: Config -> AuthResult JWTData -> Server CategoryAPI
appToServerCategory cfg auths = hoistServerWithContext categoryApi (Proxy :: Proxy '[JWT]) (convertApp cfg) (categoryServer auths)

appToServerEntry :: Config -> AuthResult JWTData -> Server EntryAPI
appToServerEntry cfg auths = hoistServerWithContext entryApi (Proxy :: Proxy '[JWT]) (convertApp cfg) (entryServer auths)

appToServerTotals :: Config -> AuthResult JWTData -> Server TotalsAPI
appToServerTotals cfg auths = hoistServerWithContext totalsApi (Proxy :: Proxy '[JWT]) (convertApp cfg) (totalsServer auths)

appToServerAuth :: Config -> Server AuthAPI
appToServerAuth cfg = hoistServer authApi (convertApp cfg) authServer

-- | This function converts our @'AppT' m@ monad into the @ExceptT ServantErr
-- m@ monad that Servant's 'enter' function needs in order to run the
-- application.
convertApp :: Config -> AppT IO a -> Handler a
convertApp cfg appt = Handler $ runReaderT (runApp appt) cfg

-- | Since we also want to provide a minimal front end, we need to give
-- Servant a way to serve a directory with HTML and JavaScript. This
-- function creates a WAI application that just serves the files out of the
-- given directory.
files :: Server Raw
files = serveDirectoryFileServer "assets"

-- | Just like a normal API type, we can use the ':<|>' combinator to unify
-- two different APIs and applications. This is a powerful tool for code
-- reuse and abstraction! We need to put the 'Raw' endpoint last, since it
-- always succeeds.
type AppAPI auths =      (Auth auths JWTData :> UserAPI)
                    :<|> (Auth auths JWTData :> GroupAPI)
                    :<|> (Auth auths JWTData :> LocationAPI)
                    :<|> (Auth auths JWTData :> CategoryAPI)
                    :<|> (Auth auths JWTData :> EntryAPI)
                    :<|> (Auth auths JWTData :> TotalsAPI)
                    :<|> AuthAPI
                    :<|> Raw

appApi :: Proxy (AppAPI '[JWT])
appApi = Proxy

-- | Finally, this function takes a configuration and runs our 'UserAPI'
-- alongside the 'Raw' endpoint that serves all of our files.
app :: Config -> Application
app cfg = serveWithContext appApi
            (defaultCookieSettings :. jwtCfg :. EmptyContext)
            (server cfg)
        where
          jwtCfg = configJWTSettings cfg

server :: Config -> Server (AppAPI auths)
server cfg =  appToServerUser     cfg :<|>
              appToServerGroup    cfg :<|>
              appToServerLocation cfg :<|>
              appToServerCategory cfg :<|>
              appToServerEntry    cfg :<|>
              appToServerTotals   cfg :<|>
              appToServerAuth     cfg :<|>
              files

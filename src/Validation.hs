module Validation (validateEntry, validateUser, validateCategory,
                   validateLocation, validateGroup, getErrorReason) where

import qualified Data.ByteString.Char8 as B
import           Data.Functor
import           Data.List
import qualified Data.Text             as DT
import           Data.Time.Clock
import           Data.Validation       as DV
import           Models                (Category (Category), Entry (Entry),
                                        Group (Group), Location (Location),
                                        User (User), categoryName,
                                        categoryParentId, entryCategoryId,
                                        entryDescription, entryEndTime,
                                        entryLocationId, entryStartTime,
                                        groupName, locationName, userAdmin,
                                        userEmail, userName, userPasswordHash)
import           Text.Email.Validate   as E

-------------------------
{- DT's for our errors -}
-------------------------
-- (Uniqueness and non-null values are already implicitly handled by the definition of the model.)
-- Data type that encapsulates every type of error
type ErrorReason   = String
data Error         = GeneralError GeneralError |
                     EntryError   EntryError   |
                     UserError    UserError

-- Data type that encapsulates General input errors
data GeneralError  = EmptyValue          { genErrorReason :: ErrorReason } |
                     NotGEQMinLength     { genErrorReason :: ErrorReason } |
                     NotLEQMaxLength     { genErrorReason :: ErrorReason } |
                     ContainsIllegalChar { genErrorReason :: ErrorReason }

-- Data type that encapsulates User specific errors
newtype UserError  = InvalidEmail        { userErrorReason :: ErrorReason }

-- Data type that encapsulates Entry specific errors
newtype EntryError = EndTimeLEQStartTime { entryErrorReason :: ErrorReason } -- |
                    -- OverlappingTimespan { entryErrorReason :: ErrorReason }

-- Datatypes for our verified entities
type ValidatedInput = Validation [Error] -- eta reduced type constructor to make it generic

type ValidatedUser     = ValidatedInput User
type ValidatedGroup    = ValidatedInput Group
type ValidatedCategory = ValidatedInput Category
type ValidatedLocation = ValidatedInput Location
type ValidatedEntry    = ValidatedInput Entry

------------------------------------
{- Top level validation functions -}
------------------------------------
validateGeneral :: String -> Int -> Int -> ValidatedInput String
validateGeneral s n1 n2 =
    nonEmptyValue  s    *>
    underMinLength s n1 *>
    overMaxLength  s n2 *>
    noIllegalChar  s    $>
    s

validateUser :: User -> ValidatedUser
validateUser u = User <$> validateUserName  sUserName  <*>
                          validateUserEmail sUserEmail <*>
                          Success (userAdmin u)        <*>
                          Success (userPasswordHash u)
    where sUserName  = DT.unpack (userName  u)
          sUserEmail = DT.unpack (userEmail u)

validateEntry :: Entry -> ValidatedEntry
validateEntry e = Entry <$> fstVal validatedTimes <*>
                            sndVal validatedTimes <*>
                            validatedDescription  <*>
                    Success (entryCategoryId e)   <*>
                    Success (entryLocationId e)
    where validatedTimes       = validateEntryTime (entryStartTime e) (entryEndTime e)
          validatedDescription = validateEntryDescription (DT.unpack (entryDescription e))

------------------------------------
{- Actual validation functions -}
------------------------------------
-- General validation
nonEmptyValue :: String -> ValidatedInput String
nonEmptyValue "" = Failure [GeneralError $ EmptyValue emptyValueMessage]
nonEmptyValue s  = Success s

underMinLength :: String -> Int -> ValidatedInput String
underMinLength s n
    | length s < n = Failure [GeneralError $ NotGEQMinLength (notGEQMinLengthMessage s n)]
    | otherwise    = Success s

overMaxLength :: String -> Int -> ValidatedInput String
overMaxLength s n
    | length s > n = Failure [GeneralError $ NotLEQMaxLength (notLEQMaxLengthMessage s n)]
    | otherwise    = Success s

noIllegalChar :: String -> ValidatedInput String
noIllegalChar s
    | containsIllegalChar = Failure [GeneralError $ ContainsIllegalChar (illegalCharMessage s)]
    | otherwise           = Success s
  where containsIllegalChar = isInfixOf illegalChars s -- isInfixOf is new 'contains' (deprecated)

illegalChars :: [Char]
illegalChars = ['/', '\\']

-- User validation functions
validateUserName :: String -> ValidatedInput DT.Text
validateUserName s =
    case validateGeneral s minUserNameLength maxUserNameLength of
      (Failure xs) -> Failure xs
      (Success x)  -> Success $ DT.pack x

validateUserEmail :: String -> ValidatedInput DT.Text
validateUserEmail s =
    validateGeneral s minEmailLength maxEmailLength *>
    isValidEmail    s                               $>
    DT.pack s

isValidEmail :: String -> ValidatedInput String
isValidEmail s = emailToVal $ E.validate (B.pack s)

emailToVal :: Either String E.EmailAddress -> ValidatedInput String
emailToVal (Left xs) = Failure [UserError (InvalidEmail xs)]
emailToVal (Right x) = let sEmail = B.unpack (toByteString x) in
                       Success sEmail

-- Group validation functions
validateGroup :: Group -> ValidatedGroup
validateGroup g =
    case validateGeneral (DT.unpack (groupName g)) minGroupNameLength maxGroupNameLength of
      (Failure xs) -> Failure xs
      (Success x)  -> Group <$> Success (DT.pack x)

-- Category validation functions
validateCategory :: Category -> ValidatedCategory
validateCategory c =
  case validateGeneral (DT.unpack (categoryName c)) minCategoryNameLength maxCategoryNameLength of
    (Failure xs) -> Failure xs
    (Success x)  -> Category <$> Success (DT.pack x) <*>
                                 Success (categoryParentId c)

-- Location validation functions
validateLocation :: Location -> ValidatedLocation
validateLocation l =
  case validateGeneral (DT.unpack (locationName l)) minLocationNameLength maxLocationNameLength of
    (Failure xs) -> Failure xs
    (Success x)  -> Location <$> Success (DT.pack x)

-- Entry validation functions
validateEntryTime :: UTCTime -> UTCTime -> ValidatedInput (UTCTime, UTCTime)
validateEntryTime t1 t2
    | t1 > t2   = Failure [EntryError $ EndTimeLEQStartTime (endTimeLEQStartTimeMessage t1 t2)]
    -- TODO: overlapping timespan check (check if for all participants there exists an entry that falls in this timespan)
    | otherwise = Success (t1, t2)

validateEntryDescription :: String -> ValidatedInput DT.Text
validateEntryDescription s =
  case validateGeneral s minEntryDescriptionLength maxEntryDescriptionLength of
    (Failure xs) -> Failure xs
    (Success x)  -> Success $ DT.pack x

endTimeLEQStartTimeMessage :: UTCTime -> UTCTime -> String
endTimeLEQStartTimeMessage t1 t2 =
    "End time " ++ show t1 ++ " is earlier than starting time " ++ show t2 ++ ".\n"

------------------------------------
{-       Field length bounds      -}
------------------------------------
-- User lengths
minUserNameLength :: Int
minUserNameLength = 3

maxUserNameLength :: Int
maxUserNameLength = 25

minEmailLength :: Int
minEmailLength = 3

maxEmailLength :: Int
maxEmailLength = 254

-- Group lengths
minGroupNameLength :: Int
minGroupNameLength = 2

maxGroupNameLength :: Int
maxGroupNameLength = 25

-- Category lengths
minCategoryNameLength :: Int
minCategoryNameLength = 2

maxCategoryNameLength :: Int
maxCategoryNameLength = 25

-- Location lengths
minLocationNameLength :: Int
minLocationNameLength = 2

maxLocationNameLength :: Int
maxLocationNameLength = 25

-- Entry lengths
minEntryDescriptionLength :: Int
minEntryDescriptionLength = 1

maxEntryDescriptionLength :: Int
maxEntryDescriptionLength = 200

------------------------------------
{-         Error messages         -}
------------------------------------
-- General error messages
emptyValueMessage :: String
emptyValueMessage = "Input is empty. "

notGEQMinLengthMessage :: String -> Int -> String
notGEQMinLengthMessage s n = "Input " ++ s ++ " doesn't meet the requirement of min. length "
                        ++ show n ++ ".\n"

notLEQMaxLengthMessage :: String -> Int -> String
notLEQMaxLengthMessage s n = "Input " ++ s ++ " doesn't meet the requirement of max. length "
                         ++ show n ++ ".\n"

illegalCharMessage :: String -> String
illegalCharMessage s = "Input " ++ s ++ " contains illegal characters.\n"

----------------------
{- Helper functions -}
----------------------
getErrorReason :: Error -> ErrorReason
getErrorReason (GeneralError ge) = genErrorReason   ge
getErrorReason (EntryError   ee) = entryErrorReason ee
getErrorReason (UserError    ue) = userErrorReason  ue

fstVal :: Validation [Error] (a, b) -> Validation [Error] a
fstVal (Failure xs)     = Failure xs
fstVal (Success (x, _)) = Success x

sndVal :: Validation [Error] (a, b) -> Validation [Error] b
sndVal (Failure ys)     = Failure ys
sndVal (Success (_, y)) = Success y

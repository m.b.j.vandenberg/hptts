{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Api.Category where

import           Control.Monad.Except        (MonadIO)
import           Data.Int                    (Int64)
import           Data.Maybe                  (fromJust, isJust)
import           Database.Persist.Postgresql (Entity (..), delete, fromSqlKey,
                                              insert, selectFirst, selectList,
                                              toSqlKey, (==.))
import           Servant
import           Servant.Auth.Server
import           Servant.JS                  (vanillaJS, writeJSForAPI)

import           Config                      (AppT (..))
import           Data.Text                   (Text)
import           Models                      (Category (Category), categoryName,
                                              categoryParentId, runDb)

import           Data.Validation
import           Validation

import qualified Models                      as Md

type CategoryAPI = "categories" :>
    (                                Get  '[JSON] [Entity Category]     -- Get all categories
    :<|> Capture "id"    Int64    :> Get  '[JSON] (Entity Category)     -- Get a specific category
    :<|> Capture "name"  Text     :> Get  '[JSON] (Entity Category)     -- Get a specific category by name
    :<|> ReqBody '[JSON] Category :> Post '[JSON]  Int64                -- Create a category
    :<|> Capture "id"    Int64    :> DeleteNoContent '[JSON] ()         -- Delete a category
    )

categoryApi :: Proxy CategoryAPI
categoryApi = Proxy

-- | The server that runs the CategoryAPI
categoryServer :: MonadIO m => AuthResult Md.JWTData -> ServerT CategoryAPI (AppT m)
categoryServer (Authenticated token) =
  allCategories         :<|>
  singleCategory        :<|>
  singleCategoryByName  :<|>
  createCategory token  :<|>
  deleteCategory token
categoryServer _                 = throwAll err401

-- | Returns all categories in the database.
allCategories :: MonadIO m => AppT m [Entity Category]
allCategories = runDb (selectList [] [])

-- | Returns a category by id or throws a 404 error.
singleCategory :: MonadIO m => Int64 -> AppT m (Entity Category)
singleCategory catId = do
    maybeCategory <- runDb (selectFirst [Md.CategoryId ==. toSqlKey catId] [])
    case maybeCategory of
         Nothing       -> throwError err404
         Just category -> return category

-- | Returns a category by name or throws a 404 error.
singleCategoryByName :: MonadIO m => Text -> AppT m (Entity Category)
singleCategoryByName str = do
    maybeCategory <- runDb (selectFirst [Md.CategoryName ==. str] [])
    case maybeCategory of
         Nothing       -> throwError err404
         Just category -> return category

-- | Creates a category in the database.
createCategory :: MonadIO m => Md.JWTData -> Category -> AppT m Int64
createCategory token c =
  if not $ Md.isadmin token then throwError err403 {errBody = "Admin required."}
  else
    case validateCategory c of
      (Failure xs) -> throwError $ err422 { errReasonPhrase = concatMap getErrorReason xs }
      (Success vc) -> do newCategory <- runDb (insert (Category (categoryName vc) (categoryParentId vc)))
                         return $ fromSqlKey newCategory

-- | Deletes a category in the database
-- TODO: fails if category is referenced by another Category or Entry,
-- I'm unsure what the recommended action is here
deleteCategory :: MonadIO m => Md.JWTData -> Int64 -> AppT m ()
deleteCategory token catId = do
  if not $ Md.isadmin token then throwError err403 {errBody = "Admin required."}
  else do
    maybeCategory <- runDb (selectFirst [Md.CategoryId ==. toSqlKey catId] [])
    if isJust maybeCategory
    then do
          let categoryKey = entityKey $ fromJust maybeCategory
          runDb (delete categoryKey)
          return ()
    else throwError err404

-- | Generates JavaScript to query the Category API.
generateJavaScript :: IO ()
generateJavaScript =
    writeJSForAPI (Proxy :: Proxy CategoryAPI) vanillaJS "./assets/category.generated.js"


{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Api.Entry where

import           Control.Monad.Except        (MonadIO)
import           Data.Int                    (Int64)
import           Data.Maybe                  (fromJust, isJust)
import           Database.Persist.Postgresql (Entity (..), delete,
                                              deleteCascade, fromSqlKey, insert,
                                              selectFirst, selectList, toSqlKey,
                                              (==.))
import           Servant
import           Servant.JS                  (vanillaJS, writeJSForAPI)

import           Config                      (AppT (..))
import           Data.Validation
import           Models                      (Entry (Entry), JWTData,
                                              Participant (Participant),
                                              entryDescription, entryEndTime,
                                              entryStartTime, isadmin, runDb)
import           Validation

-- Auth imports
import           Servant.Auth.Server

import qualified Models                      as Md

type EntryAPI = "entries" :>
    (                                Get    '[JSON] [Entity Entry]       -- Get all entries
      :<|> Capture "id"     Int64 :> Get    '[JSON] (Entity Entry)       -- Get a specific entry
      :<|> ReqBody '[JSON]  Entry :> Post   '[JSON]  Int64               -- Create an entry
      :<|> Capture "id"     Int64 :> Delete '[JSON] ()                   -- Delete an entry
      :<|> Capture "id"     Int64 :> "participants" :> Get '[JSON] [Entity Participant]        -- Get all participants of an entry
      :<|> Capture "id"     Int64 :> "participants" :> Capture "id" Int64 :> PostNoContent   '[JSON] () -- Add a participant to an entry
      :<|> Capture "id"     Int64 :> "participants" :> Capture "id" Int64 :> DeleteNoContent '[JSON] () -- Delete a participant from an entry
    )

entryApi :: Proxy EntryAPI
entryApi = Proxy

-- | The server that runs the EntryAPI
entryServer :: MonadIO m => AuthResult JWTData -> ServerT EntryAPI (AppT m)
entryServer (Authenticated user) =
  allEntries user      :<|>
  singleEntry user     :<|>
  createEntry user     :<|>
  deleteEntry user     :<|>
  allParticipants user :<|>
  addParticipant user  :<|>
  removeParticipant user

entryServer _ = throwAll err401

-- | Returns all entries in the database.
allEntries :: MonadIO m => Md.JWTData -> AppT m [Entity Entry]
allEntries _ = runDb (selectList [] [])

-- | Returns an entry by id or throws a 404 error.
singleEntry :: MonadIO m => Md.JWTData -> Int64 -> AppT m (Entity Entry)
singleEntry _ entryId = do
    maybeEntry <-
        runDb (selectFirst [Md.EntryId ==. toSqlKey entryId] [])
    case maybeEntry of
         Nothing    -> throwError err404
         Just entry -> return entry

-- | Creates an entry in the database.
createEntry :: MonadIO m => Md.JWTData -> Entry -> AppT m Int64
createEntry _ e =
    case validateEntry e of
      (Failure xs) -> throwError $ err422 { errReasonPhrase = concatMap getErrorReason xs }
      (Success ve) -> do newEntry <- runDb (insert (Entry (entryStartTime ve) (entryEndTime ve) (entryDescription ve)
                                                          (Md.entryCategoryId ve) (Md.entryLocationId ve)))
                         return $ fromSqlKey newEntry

-- | Deletes an entry in the database
deleteEntry :: MonadIO m => Md.JWTData -> Int64 -> AppT m ()
deleteEntry user entryId =
    if not (isadmin user) then throwError err401
    else do
      maybeEntry <- runDb (selectFirst [Md.EntryId ==. toSqlKey entryId] [])
      if isJust maybeEntry
      then do
            let entryKey = entityKey $ fromJust maybeEntry
            runDb (deleteCascade entryKey)
            return ()
      else throwError err404

-- | Get all participants of a entry
allParticipants :: MonadIO m => Md.JWTData -> Int64 -> AppT m [Entity Participant]
allParticipants _ entryId = runDb (selectList [Md.ParticipantEntryId ==. toSqlKey entryId] [])

-- | Add a participant to a entry
addParticipant :: MonadIO m => Md.JWTData -> Int64 -> Int64 -> AppT m ()
addParticipant _ entryId userId = do
    maybeEntry <- runDb (selectFirst [Md.EntryId ==. toSqlKey entryId] [])
    maybeUser  <- runDb (selectFirst [Md.UserId  ==. toSqlKey userId]  [])
    if isJust maybeEntry && isJust maybeUser
    then do
        let entryKey = entityKey $ fromJust maybeEntry
        let userKey  = entityKey $ fromJust maybeUser
        _ <- runDb (insert (Participant userKey entryKey))
        return ()
    else throwError err404

-- | Remove a participant from a entry
removeParticipant :: MonadIO m => Md.JWTData -> Int64 -> Int64 -> AppT m ()
removeParticipant _ entryId userId = do
    maybeParticipant <- runDb (selectFirst [Md.ParticipantEntryId ==. toSqlKey entryId,
                                            Md.ParticipantUserId  ==. toSqlKey userId] [])
    if isJust maybeParticipant
    then do
          let participantKey = entityKey $ fromJust maybeParticipant
          _ <- runDb (delete participantKey)
          return ()
    else throwError err404

-- | Generates JavaScript to query the Entry API.
generateJavaScript :: IO ()
generateJavaScript =
    writeJSForAPI (Proxy :: Proxy EntryAPI) vanillaJS "./assets/entry.generated.js"


{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Api.Group where

import           Control.Monad.Except        (MonadIO)
import           Data.Int                    (Int64)
import           Data.Maybe                  (fromJust, isJust)
import           Database.Persist.Postgresql (Entity (..), delete,
                                              deleteCascade, fromSqlKey, insert,
                                              selectFirst, selectList, toSqlKey,
                                              (==.))
import           Servant
import           Servant.JS                  (vanillaJS, writeJSForAPI)

import           Config                      (AppT (..))
import           Data.Text                   (Text)
import           Models                      (Group (Group), Member (Member),
                                              groupName, runDb)

import           Data.Validation
import           Validation

import qualified Models                      as Md
-- Auth imports
import           Models                      (JWTData, isadmin)
import           Servant.Auth.Server

type GroupAPI = "groups" :>
    (                               Get    '[JSON] [Entity Group]      -- Get all groups
      :<|> Capture "id"   Int64  :> Get    '[JSON] (Entity Group)      -- Get a specific group
      :<|> Capture "name" Text   :> Get    '[JSON] (Entity Group)      -- Get a specific group by name
      :<|> ReqBody '[JSON] Group :> Post   '[JSON] Int64
      :<|> Capture "id"   Int64  :> DeleteNoContent '[JSON] ()                  -- Delete a group
      :<|> Capture "id"   Int64  :> "members" :> Get    '[JSON] [Entity Member]             -- Get all members of a group
      :<|> Capture "id"   Int64  :> "members" :> Capture "id" Int64  :> PostNoContent   '[JSON] ()   -- Add a member to a group
      :<|> Capture "id"   Int64  :> "members" :> Capture "id" Int64  :> DeleteNoContent '[JSON] ()   -- Remove a member from a group

    )

groupApi :: Proxy GroupAPI
groupApi = Proxy

-- | The server that runs the GroupAPI
groupServer :: MonadIO m => AuthResult JWTData -> ServerT GroupAPI (AppT m)
groupServer (Authenticated user) =
  allGroups user         :<|>
  singleGroup user       :<|>
  singleGroupByName user :<|>
  createGroup user       :<|>
  deleteGroup user       :<|>
  allMembers user        :<|>
  addMember user         :<|>
  removeMember user
groupServer _ = throwAll err401

-- | Returns all groups in the database.
allGroups :: MonadIO m => JWTData -> AppT m [Entity Group]
allGroups _ = runDb (selectList [] [])

-- | Returns a group by id or throws a 404 error.
singleGroup :: MonadIO m => JWTData -> Int64 -> AppT m (Entity Group)
singleGroup _ groupId = do
    maybeGroup <- runDb (selectFirst [Md.GroupId ==. toSqlKey groupId] [])
    case maybeGroup of
         Nothing    -> throwError err404
         Just group -> return group

-- | Returns a group by name or throws a 404 error.
singleGroupByName :: MonadIO m => JWTData -> Text -> AppT m (Entity Group)
singleGroupByName _ str = do
    maybeGroup <- runDb (selectFirst [Md.GroupName ==. str] [])
    case maybeGroup of
         Nothing    -> throwError err404
         Just group -> return group

-- | Creates a group in the database.
createGroup :: MonadIO m => JWTData -> Group -> AppT m Int64
createGroup user g =
  if not (isadmin user) then throwError err401
  else
    case validateGroup g of
      (Failure xs) -> throwError $ err422 { errReasonPhrase = concatMap getErrorReason xs }
      (Success vg) -> do newGroup <- runDb (insert (Group (groupName vg)))
                         return $ fromSqlKey newGroup

-- | Deletes a group in the database
deleteGroup :: MonadIO m => JWTData -> Int64 -> AppT m ()
deleteGroup user groupId =
  if not (isadmin user) then throwError err401
  else do
    maybeGroup <- runDb (selectFirst [Md.GroupId ==. toSqlKey groupId] [])
    if isJust maybeGroup
    then do
          let groupKey = entityKey $ fromJust maybeGroup
          runDb (deleteCascade groupKey)
          return ()
    else throwError err404

-- | Get all members of a group
allMembers :: MonadIO m => JWTData -> Int64 -> AppT m [Entity Member]
allMembers _ groupId = runDb (selectList [Md.MemberGroupId ==. toSqlKey groupId] [])

-- | Add a member to a group
addMember :: MonadIO m => JWTData -> Int64 -> Int64 -> AppT m ()
addMember curUser groupId userId =
  if not (isadmin curUser) then throwError err401
  else do
    maybeGroup <- runDb (selectFirst [Md.GroupId ==. toSqlKey groupId] [])
    maybeUser  <- runDb (selectFirst [Md.UserId  ==. toSqlKey userId]  [])
    if isJust maybeGroup && isJust maybeUser
    then do
        let groupKey = entityKey $ fromJust maybeGroup
        let userKey  = entityKey $ fromJust maybeUser
        _ <- runDb (insert (Member userKey groupKey))
        return ()
    else throwError err404

-- | Remove a member from a group
removeMember :: MonadIO m => JWTData -> Int64 -> Int64 -> AppT m ()
removeMember curUser groupId userId =
  if not (isadmin curUser) then throwError err401
  else do
    maybeMember <- runDb (selectFirst [Md.MemberGroupId ==. toSqlKey groupId,
                                       Md.MemberUserId  ==. toSqlKey userId] [])
    if isJust maybeMember
    then do
          let memberKey = entityKey $ fromJust maybeMember
          _ <- runDb (delete memberKey)
          return ()
    else throwError err404

-- | Generates JavaScript to query the Group API.
generateJavaScript :: IO ()
generateJavaScript =
    writeJSForAPI (Proxy :: Proxy GroupAPI) vanillaJS "./assets/group.generated.js"


{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Api.Auth where

import           Control.Monad.Except        (MonadIO)
import           Control.Monad.Reader        (ask, liftIO)
import           Database.Persist.Postgresql (Entity (..), selectFirst, (==.))
import           GHC.Generics
import           Servant
import           Servant.Auth.Server
import           Servant.JS                  (vanillaJS, writeJSForAPI)

import           Config                      (AppT (..), configJWTSettings)
import           Crypto.KDF.BCrypt           (validatePassword)
import           Data.ByteString.Lazy.UTF8   (toString)

import           Data.Aeson.Types            (FromJSON, ToJSON)
import           Data.ByteString.Char8       (ByteString, pack)
import           Data.Text                   (Text, unpack)
import qualified Models                      as Md

data Login = Login { username :: Text, password :: Text }
   deriving (Eq, Show, Read, Generic)

instance FromJSON Login
instance ToJSON Login

type AuthAPI = "login" :>
    (
      ReqBody '[JSON] Login :> Post '[JSON] String
    )

authApi :: Proxy AuthAPI
authApi = Proxy

authServer :: MonadIO m => ServerT AuthAPI (AppT m)
authServer = getToken

-- | Schmonkel a Data.Text into a ByteString (used for BCrypt purposes).
text2BS :: Text -> ByteString
text2BS = pack . unpack

getToken :: MonadIO m => Login -> AppT m String
getToken credentials = do
  maybeUser <- Md.runDb (selectFirst [Md.UserName ==. username credentials] [])

  case maybeUser of
    Nothing -> throwError err401
    Just user ->
      if validatePassword (text2BS (password credentials)) (text2BS (Md.userPasswordHash (entityVal user)))
      then do
        config <- ask
        etoken <- liftIO $ makeJWT (Md.user2JWT user) (configJWTSettings config) Nothing

        case etoken of
          Left _  -> throwError err500
          Right t -> return $ toString t
      else throwError err401

-- | 'Protected' will be protected by 'auths', which we still have to specify.
getUser:: MonadIO m => Md.JWTData -> AppT m Md.JWTData
getUser = return

-- | Generates JavaScript to query the Auth API.
generateJavaScript :: IO ()
generateJavaScript =
    writeJSForAPI (Proxy :: Proxy AuthAPI) vanillaJS "./assets/auth.generated.js"

{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Api.Totals where

import           Control.Monad.Except        (MonadIO)
import           Data.Int                    (Int64)
import           Database.Persist.Postgresql (Entity (..))
import           Servant
import           Servant.JS                  (vanillaJS, writeJSForAPI)

import           Database.Esqueleto

import           Config                      (AppT (..))
import           Data.Time.Clock             (NominalDiffTime, UTCTime (..),
                                              diffUTCTime)
import           Models                      (Entry, JWTData, entryEndTime,
                                              entryStartTime, runDb)

import qualified Models                      as Md
-- Auth imports
import           Servant.Auth.Server


type TotalsAPI = "totals" :> QueryParam "user"     Int64
                          :> QueryParam "category" Int64
                          :> QueryParam "location" Int64
                          :> QueryParam "from"     UTCTime
                          :> QueryParam "to"       UTCTime
                          :> Get '[JSON] NominalDiffTime

totalsApi :: Proxy TotalsAPI
totalsApi = Proxy

-- | The server that runs the TotalAPI
totalsServer :: MonadIO m => AuthResult JWTData -> ServerT TotalsAPI (AppT m)
totalsServer (Authenticated _) = total
totalsServer _                 = throwAll err401

total :: MonadIO m
         => Maybe Int64 -> Maybe Int64 -> Maybe Int64 -- user, category, location
         -> Maybe UTCTime -> Maybe UTCTime            -- startTime, endTime
         -> AppT m NominalDiffTime
total mUser mCat mLoc mStart mEnd = calculateTotal <$> runDb (
    select (
    from $ \p -> do
    optionalQuery (\starttime ->
      where_ $ p ^. Md.EntryStartTime >=. val starttime)
      mStart
    optionalQuery (\endtime ->
      where_ $ p ^. Md.EntryEndTime <=. val endtime)
      mEnd
    optionalQuery (\cat ->
      where_ $ p ^. Md.EntryCategoryId ==. val (toSqlKey cat))
      mCat
    optionalQuery (\loc ->
      where_ $ p ^. Md.EntryLocationId ==. val (toSqlKey loc))
      mLoc
    optionalQuery (\user ->
      where_ $ p ^. Md.EntryId `in_`
        subList_select ( distinct $ from $
        \(entries `InnerJoin` pars) -> do
          on $ entries ^. Md.EntryId ==. pars ^. Md.ParticipantEntryId
          where_ $ pars ^. Md.ParticipantUserId ==. val (toSqlKey user)
          return $ entries ^. Md.EntryId))
      mUser
    return p))

optionalQuery :: Monad m => (t -> m ()) -> Maybe t -> m ()
optionalQuery f (Just x) = f x
optionalQuery _ Nothing  = return ()

calculateTotal :: [Entity Entry] -> NominalDiffTime
calculateTotal = sum . map timeSpent

timeSpent :: Entity Entry -> NominalDiffTime
timeSpent (Entity _ x) = entryEndTime x `diffUTCTime` entryStartTime x

-- | Generates JavaScript to query the Category API.
generateJavaScript :: IO ()
generateJavaScript =
    writeJSForAPI (Proxy :: Proxy TotalsAPI) vanillaJS "./assets/total.generated.js"


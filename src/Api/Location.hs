{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Api.Location where

import           Control.Monad.Except        (MonadIO)
import           Data.Int                    (Int64)
import           Data.Maybe                  (fromJust, isJust)
import           Database.Persist.Postgresql (Entity (..), delete, fromSqlKey,
                                              insert, selectFirst, selectList,
                                              toSqlKey, (==.))
import           Servant
import           Servant.Auth.Server
import           Servant.JS                  (vanillaJS, writeJSForAPI)

import           Config                      (AppT (..))
import           Data.Text                   (Text)
import           Models                      (JWTData, Location (Location),
                                              isadmin, locationName, runDb)

import           Data.Validation
import           Validation

import qualified Models                      as Md

type LocationAPI = "locations" :>
    (                                  Get  '[JSON] [Entity Location]    -- Get all locations
      :<|> Capture "id"    Int64    :> Get  '[JSON] (Entity Location)    -- Get a specific location
      :<|> Capture "name"  Text     :> Get  '[JSON] (Entity Location)    -- Get a specific location by name
      :<|> ReqBody '[JSON] Location :> Post '[JSON] Int64                -- Create a new location
      :<|> Capture "id"    Int64    :> DeleteNoContent '[JSON] ()        -- Delete a location
    )

locationApi :: Proxy LocationAPI
locationApi = Proxy

-- | The server that runs the LocationAPI
locationServer :: MonadIO m => AuthResult Md.JWTData -> ServerT LocationAPI (AppT m)
locationServer (Authenticated token) =
  allLocations            :<|>
  singleLocation          :<|>
  singleLocationByName    :<|>
  createLocation token    :<|>
  deleteLocation token
locationServer _ = throwAll err401

-- | Returns all locations in the database.
allLocations :: MonadIO m => AppT m [Entity Location]
allLocations = runDb (selectList [] [])

-- | Returns a location by id or throws a 404 error.
singleLocation :: MonadIO m => Int64 -> AppT m (Entity Location)
singleLocation locId = do
    maybeLocation <- runDb (selectFirst [Md.LocationId ==. toSqlKey locId] [])
    case maybeLocation of
         Nothing       -> throwError err404
         Just location -> return location

-- | Returns a location by name or throws a 404 error.
singleLocationByName :: MonadIO m => Text -> AppT m (Entity Location)
singleLocationByName str = do
    maybeLocation <- runDb (selectFirst [Md.LocationName ==. str] [])
    case maybeLocation of
         Nothing       -> throwError err404
         Just location -> return location

-- | Creates a location in the database.
createLocation :: MonadIO m => JWTData -> Location -> AppT m Int64
createLocation token l =
  if not $ isadmin token then throwError err403 {errBody = "Admin required."}
  else
    case validateLocation l of
      (Failure xs) -> throwError $ err422 { errReasonPhrase = concatMap getErrorReason xs }
      (Success vl) -> do newLocation <- runDb (insert (Location (locationName vl)))
                         return $ fromSqlKey newLocation

-- | Deletes a location in the database
-- TODO: fails if location is referenced by an Entry,
-- I'm unsure what the recommended action is here
deleteLocation :: MonadIO m => JWTData -> Int64 -> AppT m ()
deleteLocation token locId = do
  if not $ isadmin token then throwError err403 {errBody = "Admin required."}
  else do
    maybeLocation <- runDb (selectFirst [Md.LocationId ==. toSqlKey locId] [])
    if isJust maybeLocation
    then do
          let locationKey = entityKey $ fromJust maybeLocation
          runDb (delete locationKey)
          return ()
    else throwError err404

-- | Generates JavaScript to query the Location API.
generateJavaScript :: IO ()
generateJavaScript =
    writeJSForAPI (Proxy :: Proxy LocationAPI) vanillaJS "./assets/location.generated.js"

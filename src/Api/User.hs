{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Api.User where

import           Control.Monad.Except        (MonadIO, liftIO)
import           Data.Int                    (Int64)
import           Data.Maybe                  (fromJust, isJust)
import           GHC.Generics

import           Database.Persist.Postgresql (Entity (..), deleteCascade,
                                              fromSqlKey, insert, selectFirst,
                                              selectList, toSqlKey, updateWhere,
                                              (=.), (==.))
import           Servant
import           Servant.JS                  (vanillaJS, writeJSForAPI)

import           Config                      (AppT (..))
import           Data.Text                   (Text)
import qualified Data.Text                   as DT
import           Models                      (User (User), runDb, userAdmin,
                                              userEmail, userName)

import           Data.Aeson.Types            (FromJSON, ToJSON)
import           Data.Validation
import           Validation
-- Auth imports
import           Crypto.KDF.BCrypt           (bcrypt, validatePassword)
import qualified Crypto.Random.Types         as CRT
import qualified Data.ByteString.Char8       as B
import qualified Data.ByteString.Random.MWC  as DBRM
import           Models                      (JWTData, isadmin)
import           Servant.Auth.Server

import qualified Models                      as Md

type UserAPI = "users" :>
    (                             Get    '[JSON] [Entity User]            -- Get all users
    :<|> Capture "id"    Int64 :> Get    '[JSON] (Entity User)            -- Get a specific user by its id
    :<|> Capture "id"    Int64 :> DeleteNoContent '[JSON] ()
    :<|> Capture "name"  Text  :> Get    '[JSON] (Entity User)            -- Get a specific user by its name
    :<|> ReqBody '[JSON] User  :> Post   '[JSON]  Int64                   -- Create a new user
    :<|> "changePassword" :> ReqBody '[JSON] PwChange :> PostNoContent '[JSON] () -- Update current user's password
    :<|> "resetPassword" :> Capture "name" Text :> ReqBody '[JSON] String :> PostNoContent '[JSON] () -- Forcibly reset user's password
    )

userApi :: Proxy UserAPI
userApi = Proxy

-- | The server that runs the UserAPI
userServer :: (MonadIO m, CRT.MonadRandom m) => AuthResult JWTData -> ServerT UserAPI (AppT m)
userServer (Authenticated user) =
  allUsers user         :<|>
  singleUser user       :<|>
  deleteUser user       :<|>
  singleUserByName user :<|>
  createUser user       :<|>
  changePasswd user     :<|>
  resetPasswd user
userServer _ = throwAll err401

-- | Returns all users in the database.
allUsers :: MonadIO m => JWTData -> AppT m [Entity User]
allUsers _ = runDb (selectList [] [])

-- | Returns a user by id or throws a 404 error.
singleUser :: MonadIO m => JWTData -> Int64 -> AppT m (Entity User)
singleUser curUser userId =
  if not (isadmin curUser) then throwError err401
  else do
    maybeUser <- runDb (selectFirst [Md.UserId ==. toSqlKey userId] [])
    case maybeUser of
         Nothing ->
            throwError err404
         Just person ->
            return person

-- | Returns a user by name or throws a 404 error.
singleUserByName :: MonadIO m => JWTData -> Text -> AppT m (Entity User)
singleUserByName _ str = do
    maybeUser <- runDb (selectFirst [Md.UserName ==. str] [])
    case maybeUser of
         Nothing ->
            throwError err404
         Just person ->
            return person

-- | Creates a user in the database.
createUser :: (MonadIO m, CRT.MonadRandom m) => JWTData -> User -> AppT m Int64
createUser curUser u =
  if not (isadmin curUser) then throwError err401
  else
    case validateUser u of
      (Failure xs) -> throwError $ err422 { errReasonPhrase = concatMap getErrorReason xs }
      (Success vu) -> do
        let password = B.pack $ DT.unpack $ Md.userPasswordHash vu
        salt <- liftIO $ DBRM.random 16
        let pwhash = (bcrypt 10 salt password :: B.ByteString)
        let pwhashText = DT.pack $ B.unpack pwhash
        newUser <- runDb (insert (User (userName vu) (userEmail vu) (userAdmin vu) pwhashText))
        return $ fromSqlKey newUser

-- | Deletes an User in the database
deleteUser :: MonadIO m => JWTData -> Int64 -> AppT m ()
deleteUser curUser userId =
  if not (isadmin curUser) then throwError err401
  else do
    maybeUser <- runDb (selectFirst [Md.UserId ==. toSqlKey userId] [])
    if isJust maybeUser
    then do
          let userKey = entityKey $ fromJust maybeUser
          runDb (deleteCascade userKey)
          return ()
    else throwError err404

-- | Changes the current user's password
changePasswd :: MonadIO m => JWTData -> PwChange -> AppT m ()
changePasswd curUser pwChange = do
  let idToUpdate = Md.userid curUser
  maybeUser <- runDb (selectFirst [Md.UserId ==. idToUpdate] [])
  case maybeUser of
    Nothing -> throwError err403 {errBody = "User does not exist. Huh?"}
    Just userModel -> do
      let passwordHash = (B.pack (DT.unpack (Md.userPasswordHash (entityVal userModel))) :: B.ByteString)
      let passwordBytes = (B.pack (oldPassword pwChange))
      if not (validatePassword passwordBytes passwordHash)
      then throwError err403 {errBody = "Old password incorrect."}
      else do
        if null (newPassword pwChange)
        then throwError err400 {errBody = "New password cannot be empty."}
        else do
          salt <- liftIO $ DBRM.random 16
          let newHash = (bcrypt 10 salt (B.pack (newPassword pwChange)) :: B.ByteString)
          let newHashText = DT.pack $ B.unpack newHash
          runDb $ updateWhere [Md.UserId ==. idToUpdate] [Md.UserPasswordHash =. newHashText]
          return ()

data PwChange = PwChange {oldPassword :: String, newPassword :: String}
  deriving (Eq, Show, Read, Generic)

-- | Forcibly reset a user's password
resetPasswd :: MonadIO m => JWTData -> Text -> String -> AppT m ()
resetPasswd curUser targetUserName newPasswd = do
  if not (Md.isadmin curUser) then throwError err403 {errBody = "Administrator required"}
  else do
    maybeUser <- runDb $ selectFirst [Md.UserName ==. targetUserName] []
    case maybeUser of
      Nothing -> throwError err404 {errBody = "User does not exist."}
      Just _ -> do
        if null newPasswd then throwError err400 {errBody = "New password cannot be empty."}
        else do
          salt <- liftIO $ DBRM.random 16
          let newHash = DT.pack $ B.unpack (bcrypt 10 salt (B.pack newPasswd) :: B.ByteString)
          runDb $ updateWhere [Md.UserName ==. targetUserName] [Md.UserPasswordHash =. newHash]
          return ()

instance ToJSON PwChange
instance FromJSON PwChange

-- | Generates JavaScript to query the User API.
generateJavaScript :: IO ()
generateJavaScript =
    writeJSForAPI (Proxy :: Proxy UserAPI) vanillaJS "./assets/user.generated.js"

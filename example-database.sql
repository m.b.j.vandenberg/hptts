insanelysecret
-- used as password if used with psql. Remove this line if you're importing into adminer.
-- Adminer 4.7.1 PostgreSQL dump

\connect "hptts";

DROP TABLE IF EXISTS "category";
DROP SEQUENCE IF EXISTS category_id_seq;
CREATE SEQUENCE category_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 4 CACHE 1;

CREATE TABLE "public"."category" (
    "id" bigint DEFAULT nextval('category_id_seq') NOT NULL,
    "name" character varying NOT NULL,
    "parent_id" bigint,
    CONSTRAINT "category_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "unique_category_name" UNIQUE ("name"),
    CONSTRAINT "category_parent_id_fkey" FOREIGN KEY (parent_id) REFERENCES category(id) NOT DEFERRABLE
) WITH (oids = false);

INSERT INTO "category" ("id", "name", "parent_id") VALUES
(1,	'Meetings',	NULL),
(2,	'Lectures',	NULL),
(3,	'Research',	NULL),
(4,	'Coding',	NULL);

DROP TABLE IF EXISTS "entry";
DROP SEQUENCE IF EXISTS entry_id_seq;
CREATE SEQUENCE entry_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 3 CACHE 1;

CREATE TABLE "public"."entry" (
    "id" bigint DEFAULT nextval('entry_id_seq') NOT NULL,
    "start_time" timestamptz NOT NULL,
    "end_time" timestamptz NOT NULL,
    "description" character varying NOT NULL,
    "category_id" bigint NOT NULL,
    "location_id" bigint NOT NULL,
    CONSTRAINT "entry_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "entry_category_id_fkey" FOREIGN KEY (category_id) REFERENCES category(id) NOT DEFERRABLE,
    CONSTRAINT "entry_location_id_fkey" FOREIGN KEY (location_id) REFERENCES location(id) NOT DEFERRABLE
) WITH (oids = false);

INSERT INTO "entry" ("id", "start_time", "end_time", "description", "category_id", "location_id") VALUES
(1,	'2019-01-25 11:30:00+00',	'2019-01-25 12:45:00+00',	'Meeting',	1,	2),
(2,	'2019-01-18 11:30:00+00',	'2019-01-18 12:45:00+00',	'Meeting',	1,	2),
(3,	'2019-01-18 11:30:00+00',	'2019-01-18 12:45:00+00',	'Implementing the StringBeanFactoryExceptionInterface',	4,	3);

DROP TABLE IF EXISTS "group";
DROP SEQUENCE IF EXISTS group_id_seq;
CREATE SEQUENCE group_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 5 CACHE 1;

CREATE TABLE "public"."group" (
    "id" bigint DEFAULT nextval('group_id_seq') NOT NULL,
    "name" character varying NOT NULL,
    CONSTRAINT "group_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "unique_group_name" UNIQUE ("name")
) WITH (oids = false);

INSERT INTO "group" ("id", "name") VALUES
(1,	'Team front-end'),
(2,	'Team back-end'),
(3,	'Management'),
(4,	'Everyone');

DROP TABLE IF EXISTS "location";
DROP SEQUENCE IF EXISTS location_id_seq;
CREATE SEQUENCE location_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 3 CACHE 1;

CREATE TABLE "public"."location" (
    "id" bigint DEFAULT nextval('location_id_seq') NOT NULL,
    "name" character varying NOT NULL,
    CONSTRAINT "location_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "unique_location_name" UNIQUE ("name")
) WITH (oids = false);

INSERT INTO "location" ("id", "name") VALUES
(1,	'University'),
(2,	'Group space'),
(3,	'Home');

DROP TABLE IF EXISTS "member";
DROP SEQUENCE IF EXISTS member_id_seq;
CREATE SEQUENCE member_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START  CACHE 1;

CREATE TABLE "public"."member" (
    "id" bigint DEFAULT nextval('member_id_seq') NOT NULL,
    "user_id" bigint NOT NULL,
    "group_id" bigint NOT NULL,
    CONSTRAINT "member_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "unique_member" UNIQUE ("user_id", "group_id"),
    CONSTRAINT "member_group_id_fkey" FOREIGN KEY (group_id) REFERENCES "group"(id) NOT DEFERRABLE,
    CONSTRAINT "member_user_id_fkey" FOREIGN KEY (user_id) REFERENCES "user"(id) NOT DEFERRABLE
) WITH (oids = false);


DROP TABLE IF EXISTS "participant";
DROP SEQUENCE IF EXISTS participant_id_seq;
CREATE SEQUENCE participant_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 6 CACHE 1;

CREATE TABLE "public"."participant" (
    "id" bigint DEFAULT nextval('participant_id_seq') NOT NULL,
    "user_id" bigint NOT NULL,
    "entry_id" bigint NOT NULL,
    CONSTRAINT "participant_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "unique_participant" UNIQUE ("user_id", "entry_id"),
    CONSTRAINT "participant_entry_id_fkey" FOREIGN KEY (entry_id) REFERENCES entry(id) NOT DEFERRABLE,
    CONSTRAINT "participant_user_id_fkey" FOREIGN KEY (user_id) REFERENCES "user"(id) NOT DEFERRABLE
) WITH (oids = false);

INSERT INTO "participant" ("id", "user_id", "entry_id") VALUES
(1,	1,	1),
(2,	2,	1),
(3,	3,	1),
(4,	1,	2),
(5,	3,	2),
(6,	2,	3);

DROP TABLE IF EXISTS "user";
DROP SEQUENCE IF EXISTS user_id_seq;
CREATE SEQUENCE user_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 3 CACHE 1;

CREATE TABLE "public"."user" (
    "id" bigint DEFAULT nextval('user_id_seq') NOT NULL,
    "name" character varying NOT NULL,
    "email" character varying NOT NULL,
    "admin" boolean NOT NULL,
    "password_hash" character varying NOT NULL,
    CONSTRAINT "unique_email" UNIQUE ("email"),
    CONSTRAINT "unique_user_name" UNIQUE ("name"),
    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "user" ("id", "name", "email", "admin", "password_hash") VALUES
(1,	'admin',	'admin@example.com',	'1',	'$2b$10$539habMw513yr1KB06OyE.j3LcsVpzaqg5uxPJXWi4fyoY5NrDdo.'),
(2,	'user01',	'user01@example.com',	'0',	'$2b$10$4oVg9rL29EgZVIY22/eqZ.IJ0VL89k1dVrcWdv65/PHE3oNPuzZSK'),
(3,	'user02',	'user02@example.com',	'0',	'$2b$10$n9afg2TPeJQjSEzR2MSOtO2tggmXrgcm.g8Eb.66m0W6R4xqJgqNS');

-- 2019-04-19 19:53:29.838399+00
